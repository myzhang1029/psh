# psh
[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/myzhang1029/psh) 

## Introduction
A simple simulation of bash and dash based on wshell for learning.

## Usage
type

	make

for psh with readline library. It will require libreadline.


### Attention
typing file's names such as "example.[oa]" are not supported.

## File List
### main.c
	main prog

### show.c
	print out prompt

### input.c
	read input

### builtins.c builtins/
	part of the builtins

### parser.c
	translates input
